function Nhap_du_lieu() {

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet_input   = ss.getSheetByName("input");
  var sheet_summary   = ss.getSheetByName("Summary");
  var bom1   = ss.getSheetByName("BOM 1");
  var bom2   = ss.getSheetByName("BOM 2");
  var bom3   = ss.getSheetByName("BOM 3");
  var bom4   = ss.getSheetByName("BOM 4");


  var sm_button = sheet_input.getRange(2,6,1,1).getValue();
  if((sm_button == "Nhập"))
   {
      sheet_input.getRange(2, 7, 1, 1).setValue("Processing...");
      myFunction();
      Utilities.sleep(400)
      sheet_input.getRange(2,6,1,1).setValue("");
      sheet_input.getRange(2, 7, 1, 1).setValue("");
      }
    }
 
function nhap(){
var banggiaID=Get_value(0,2,2,1,1);
var numsheet=Get_value(0,3,2,1,1);
if(banggiaID!="")
if(numsheet>0)
myFunction();
else 
 {var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet_input   = ss.getSheetByName("input");
  sheet_input.getRange(2,6,1,1).setValue("Bang gia ID hoac sheet number khong dung");}
}
function myFunction(){
  var dataclear=Create_array(100);
  for(var i=0;i<100;i++)
    for(var j=0;j<8;j++)
    dataclear[i][j]=""
   WRITE_DATA_1("Summary",6,2,dataclear.length,8,dataclear);
 
  var u=0;
  var banggiaID=Get_value(0,2,2,1,1);
  var numsheet=Get_value(0,3,2,1,1);
  var data=Create_array(100);
  for(var i=2;i<numsheet+2;i++)
   {for(var k=0;k<100;k++)
     if(data[k][0]==null)
    break;
    var data1=GET_DATA_1(i);
    var data=add_BOM(data,data1,k);}
//Logger.log(data)}
//Logger.log(data)
  var u=0;
  for(var i=0;i<data.length;i++)
    if(data[i][5]!=0)
     if(data[i][3]!="")
      u++;
  var dataT=Create_array(100);
//Logger.log(u)
  for(var i=0;i<data.length;i++)
    {for(var j=0;j<6;j++)
      dataT[i][j]=data[i][j]
    }
  for(var i=0;i<dataT.length;i++)
    {dataT[i][6]=0;
     dataT[i][7]=0;}
  
  var price=GET_price("1GXmdIGn9ac-7zoAd61hNntNP6Rg1IiOJG9TyqRsTxUI","Xuat_nhap");
  
  dataT=XL(dataT,2,8);
  price=XL(price,0,7);
  
  dataT=process_price(dataT,price);
//Logger.log(dataT)

  WRITE_DATA_1("Summary",6,2,dataT.length,8,dataT);

}
function  Get_value(numsheet,start_row,start_column,numrow,numcolumn)
{
var codeApp = SpreadsheetApp.getActiveSpreadsheet()
var sheet = codeApp.getSheets()[numsheet]   ;   
var range=sheet.getRange(start_row,start_column,numrow,numcolumn)
var value=range.getValue();
return value;
}
function  GET_DATA_1(numsheet) {
 var codeApp = SpreadsheetApp.getActiveSpreadsheet()
      var sheet = codeApp.getSheets()[numsheet]   ;   
      var lastColumn = sheet.getLastColumn();
      var lastRow = sheet.getLastRow();
      var range = sheet.getRange(1,1,lastRow,lastColumn);
      var k=["Part type","Comment","Description","Part Number","Manufacturer","Quantity"]
      var d = range.getValues(); //Logger.log(lastRow)
      var data=Create_array(lastRow);
      for(var u=0;u<6;u++)
      for(var i=0;i<lastColumn;i++)
      if(d[1][i]==k[u])
      for(var j=2;j<lastRow;j++)
      {
      data[j-2][u]=d[j][i];}
      Logger.log("data"+numsheet)
      Logger.log(data);
      return data;
 }

 function GET_DATA_2(googleSheetId,sheetName){  
      var data =[[]];     
      var codeApp = SpreadsheetApp.openByUrl(googleSheetId);
      var sheet = codeApp.getSheetByName(sheetName);       
      var lastColumn = sheet.getLastColumn();
      var lastRow = sheet.getLastRow();
      var range = sheet.getRange(1,1,lastRow,lastColumn);
      var k=["Part type","Comment","Description","Part Number","Manufacturer","Quantity"]
      var d = range.getValues(); 
      var data=Create_array(lastRow);
      for(var u=0;u<7;u++)
      for(var i=0;i<lastColumn;i++)
      if(d[4][i]==k[u])
      for(var j=6;j<lastRow;j++)
      {
      data[j-6][u]=d[j][i];}
      //Logger.log(data)
      return data;
}
 function GET_price(googleSheetId,sheetName){  
      var data =[[]];     
      var codeApp = SpreadsheetApp.openById(googleSheetId);
      var sheet = codeApp.getSheetByName(sheetName);       
      var lastColumn = sheet.getLastColumn();
      var lastRow = sheet.getLastRow();
      var range = sheet.getRange(1,1,lastRow,lastColumn);
      var k=["Part Number",10,50,100,500,1000,">1000"]
      var d = range.getValues(); 
      var data=Create_array(lastRow);
      for(var u=0;u<7;u++)
      for(var i=0;i<lastColumn;i++)
      if(d[5][i]==k[u])
      for(var j=6;j<lastRow;j++)
      {
      data[j-6][u]=d[j][i];}
      Logger.log(data)
      return data;
      }
function WRITE_DATA_1(namesheet,row, column, numRows, numColumns,data){
 var codeApp= SpreadsheetApp.getActiveSpreadsheet();
 var sheet= codeApp.getSheetByName(namesheet);
 var rangetocopy= sheet.getRange(row, column, numRows, numColumns);
 rangetocopy.setValues(data);
 }
function WRITE_DATA_2(googleSheetId,sheetName,row, column, numRows, numColumns,data)
{
 var data =[[]];     
 var codeApp = SpreadsheetApp.openById(googleSheetId);
 var sheet = codeApp.getSheetByName(sheetName);
 var rangetocopy= sheet.getRange(row, column, numRows, numColumns);
 rangetocopy.setValues(data);

}
 function process_price(dataT,price){
  var quan=[0,10,50,100,500,1000];
  Logger.log(price)
  Logger.log(dataT)
  for(var i=0;i<dataT.length;i++)
  for(var j=0;j<price.length;j++)
  if(dataT[i][3]==price[j][0])
  for(var l=0;l<6;l++)
  if(dataT[i][5]>quan[l])
  {if(price[j][l]!=null)
  {dataT[i][6]=price[j][l+1]
  dataT[i][7]=dataT[i][6]*dataT[i][5]}}
  return dataT;
 

}
function add_BOM(data,data1,k){
  
  for(var j=1;j<data1.length;j++)
  {var u=1;
  for(var i=1;i<data.length;i++)
  
  if(data[i][3]==data1[j][3])
  {u=0;
  data[i][5]=data[i][5]+data1[j][5];
  break;
  }
  if(data1[j][3]!="")
  if(u==1)
  {for(var m=0;m<6;m++)
  data[k][m]=data1[j][m];
  k++;
  }}
  return data;
  
}

function Create_array(numcolumn){
var x=[];
var x=[];
for(var i=0;i<numcolumn;i++)
x[i]=[];
return x;
}
function XL(data,r,x)
{var u=0;
for(var i=0;i<data.length;i++)
if (data[i][r]!=null)
u++
var d=Create_array(u);
for(var j=0;j<u;j++)
for(var k=0;k<x;k++)
d[j][k]=data[j][k];
return d;

}

